<?php
add_action( 'rest_api_init', 'add_custom_rest_fields' );
function add_custom_rest_fields() {
    // schema for the bs_author_name field
    $schema_video_url = array(
        'description'   => 'URL of featured video',
        'type'          => 'string',
        'context'       => ['view']
    );

    // registering the bs_author_name field
    register_rest_field(
        'post',
        'featured_video_url',
        array(
            'get_callback'      => 'get_post_video',
            'update_callback'   => 'get_post_video',
            'schema'            => $schema_video_url
        )
    );
}

function get_post_video() {
    return get_the_post_video_url();
}