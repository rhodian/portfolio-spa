<?php
/*
Plugin Name: Featured Video Plus REST
Plugin URI: http://yrnxt.com/wordpress/featured-video-plus/
Description: Adds a `featured_video_url` to post endpoint data
Version: 1.0.0
Author: Helliax
Author URI: http://www.helliax.com
Text Domain: featured-video-plus-rest
License: MIT

	Copyright 2009-2016  Alexander Höreth (email: a.hoereth@gmail.com)

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License version 2,
	as published by the Free Software Foundation.

	You may NOT assume that you can use any other version of the GPL.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	The license for this software can likely be found here:
	http://www.gnu.org/licenses/gpl-2.0.html

*/

add_action( 'rest_api_init', 'add_custom_rest_fields' );
function add_custom_rest_fields() {
    // schema for the bs_author_name field
    $schema_video_url = array(
        'description'   => 'URL of featured video',
        'type'          => 'string',
        'context'       => ['view']
    );

    // registering the bs_author_name field
    register_rest_field(
        'post',
        'featured_video_url',
        array(
            'get_callback'      => 'get_post_video',
            'update_callback'   => 'get_post_video',
            'schema'            => $schema_video_url
        )
    );
}

function get_post_video() {
    return get_the_post_video_url();
}