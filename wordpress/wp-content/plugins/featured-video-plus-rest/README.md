# Featured Video Plus REST API

Adds a `featured_video_url` to `post` REST data

Requires the [Featured Video Plus](https://wordpress.org/support/plugin/featured-video-plus) plugin.